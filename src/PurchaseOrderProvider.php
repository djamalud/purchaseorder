<?php

namespace Mangrupa\PurchaseOrder;

use Illuminate\Support\ServiceProvider;

class PurchaseOrderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ .'/routes.php');
    }
}
